# Stratified Granger Causality

## Overview

In this project, we will see how to expand Granger's Causality detection to non-linear coupling between time series. 


### Prerequisites

What things you need to install the software and how to install them

```
Scipy (0.18 or later)
Numpy (1.11 or later)
statsmodels (0.6 or later)
```


## Running the tests

You will find an [Ipython Notebook](https://gitlab.com/SBINX/stratifiedGC/blob/master/example.ipynb) that will explain how to use this library. 
It will:
* Compare the results of this modification of GC against the original implementation
 

## Authors

* **Seddik BELKOURA** 


## Future Work

This work is still in progress. The result shown are very encouraging. The author is currently working on:
* optimizing/automatize the choice of the stratification to boost the results.
* Assess the necessity of an early stop trigger when causality is detected.
* Test more configuration to see how robust is this method



## License

